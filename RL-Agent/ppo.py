# docs and experiment results can be found at https://docs.cleanrl.dev/rl-algorithms/ppo/#ppopy

import argparse
import os
import random
import time
from distutils.util import strtobool
import gym
import numpy as np
import math
import torch
import torch.nn as nn
import torch.optim as optim
from torch.distributions.categorical import Categorical
from torch.utils.tensorboard import SummaryWriter
from gym import spaces
from huggingface_hub import HfApi, upload_folder
from huggingface_hub.repocard import metadata_eval_result, metadata_save
from pathlib import Path
import datetime
import tempfile
import json
import shutil
import imageio
import subprocess
from wasabi import Printer
import asyncio
import websockets
import json
from hugging_face import hub
from Agent import Agent
from gym.vector import SyncVectorEnv
import wandb
import math
msg = Printer()
hub=hub()
#observation=np.array([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4.0, 4.0, 4.0, 4.0, 0.0])
observation=np.array([0, 0, 0])
observation_queue = asyncio.Queue(1)
step_to_observ=[]
"""//////////////// da ultimare/////////////"""

#response = {'message': "heal"}

class CustomEnv(gym.Env):
    def __init__(self):
        super(CustomEnv, self).__init__()
        low_range = np.array([0, -50.0, -50.0, -180, -50.0, -50.0, -50.0, -50.0, -50.0, -50.0, 0.0, 0.0, 0.0, 0.0, 0.0])[1:3]
        high_range = np.array([0, 50.0, 50.0, 180, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 4.0, 4.0, 4.0, 4.0, 100.0])[1:3]
        self.observation_space = spaces.Box(low=low_range, high=high_range, dtype=np.float32)
        self.action_space = spaces.Discrete(4)  # 6 possible actions
        self.current_state = np.array([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4.0, 4.0, 4.0, 4.0, 0.0])[1:3]
        #self.target_value=self.current_state[3:5]
        self.dist_new=0
        self.prev_dist=0
        self.obs_lock = asyncio.Lock()
        self.response = {'message': "heal"}
        
    def reset(self):
        self.response = {'message': "heal"}
        return self.current_state
        
    def get_response(self):
        print("SENDING----->",self.response['message'])
        return self.response
        
    def get_current(self):
        return self.current_state
        
    def step(self, action):
        
        prev_obs = self.current_state.copy()
        new_observation = self.current_state.copy()
        self.prev_dist=self.dist_new
        print("STATE--->",self.current_state)
        
        if action.item() == 0:
            self.response['message']="up"
            #print(new_observation[0] + 0.8*math.sin(math.atan2(new_observation[1], new_observation[0])))
            new_observation[0]= new_observation[0] + 0.8*math.sin(math.atan2(new_observation[1], new_observation[0]))
            new_observation[1]= new_observation[1] + 0.8*math.cos( math.atan2(new_observation[1], new_observation[0]))
        elif action.item() == 1:
            self.response['message']="down"
            new_observation[0]= new_observation[0] - 0.8*math.sin(math.atan2(new_observation[1], new_observation[0]))
            new_observation[1]= new_observation[1] - 0.8*math.cos(math.atan2(new_observation[1], new_observation[0]))
        elif action.item() == 2:
            self.response['message']="left"
            #new_observation[2]=new_observation[2].item()+0.3
        elif action.item() == 3:
            self.response['message']="right"
            #new_observation[2]=new_observation[2].item()-0.3
        elif action.item() == 4:
            self.response['message']="blade"
        elif action.item() == 5:
            self.response['message']="shield"
        elif action.item() == 6:
            self.response['message']="heal"
        elif action.item() == 7:
            print("resetting...")
            self.response['message']="reset"
        
        self.dist_new=(prev_obs[0]*prev_obs[0]-self.target_value[0]*self.target_value[0])+(prev_obs[1]*prev_obs[1]-self.target_value[1]*self.target_value[1])
        done = abs(sum(new_observation - self.target_value)) < 1
        if self.prev_dist==0:
            self.prev_dist=self.dist_new
        reward = self.prev_dist-self.dist_new
        if self.response['message']=="left" or self.response['message']=="right":
            reward-=50
        elif self.response['message']=="down" or self.response['message']=="up":
            reward-=10
        #print("STATE--->",self.current_state)
        
        return self.current_state, reward, done, {}
        
    def set_obs(self,obs):
        self.current_state= obs
        self.target_value= np.array([35,35])#obs[3:5]
                 
    def render(self, mode='human'):
        pass
        
        
# Subclass SyncVectorEnv and override necessary methods
class CustomSyncVectorEnv(SyncVectorEnv):
    def __init__(self, *args, **kwargs):
        super(CustomSyncVectorEnv, self).__init__(*args, **kwargs)

    def get_response(self):
        responses = [env.get_response() for env in self.envs]
        return responses
        
    def get_current(self):
        current_states = [env.get_current() for env in self.envs]
        return current_states
        
    def set_obs(self, obs):
        for env in self.envs:
            env.set_obs(obs)
    
    

def parse_args():
    # fmt: off
    
    parser = argparse.ArgumentParser()
    parser.add_argument("--exp-name", type=str, default=os.path.basename(__file__).rstrip(".py"),
        help="the name of this experiment")
    parser.add_argument("--seed", type=int, default=1,
        help="seed of the experiment")
    parser.add_argument("--torch-deterministic", type=lambda x: bool(strtobool(x)), default=True, nargs="?", const=True,
        help="if toggled, `torch.backends.cudnn.deterministic=False`")
    parser.add_argument("--cuda", type=lambda x: bool(strtobool(x)), default=True, nargs="?", const=True,
        help="if toggled, cuda will be enabled by default")
    parser.add_argument("--track", type=lambda x: bool(strtobool(x)), default=False, nargs="?", const=True,
        help="if toggled, this experiment will be tracked with Weights and Biases")
    parser.add_argument("--wandb-project-name", type=str, default="cleanRL",
        help="the wandb's project name")
    parser.add_argument("--wandb-entity", type=str, default=None,
        help="the entity (team) of wandb's project")
    parser.add_argument("--capture-video", type=lambda x: bool(strtobool(x)), default=False, nargs="?", const=True,
        help="weather to capture videos of the agent performances (check out `videos` folder)")
    parser.add_argument("--env-id", type=str, default="CartPole-v1",
        help="the id of the environment")
    parser.add_argument("--total-timesteps", type=int, default=50000,
        help="total timesteps of the experiments")
    parser.add_argument("--learning-rate", type=float, default=5e-3,
        help="the learning rate of the optimizer")
    parser.add_argument("--num-envs", type=int, default=1,
        help="the number of parallel game environments")
    parser.add_argument("--num-steps", type=int, default=1024,
        help="the number of steps to run in each environment per policy rollout")
    parser.add_argument("--anneal-lr", type=lambda x: bool(strtobool(x)), default=True, nargs="?", const=True,
        help="Toggle learning rate annealing for policy and value networks")
    parser.add_argument("--gae", type=lambda x: bool(strtobool(x)), default=True, nargs="?", const=True,
        help="Use GAE for advantage computation")
    parser.add_argument("--gamma", type=float, default=0.99,
        help="the discount factor gamma")
    parser.add_argument("--gae-lambda", type=float, default=0.95,
        help="the lambda for the general advantage estimation")
    parser.add_argument("--num-minibatches", type=int, default=64,
        help="the number of mini-batches")
    parser.add_argument("--update-epochs", type=int, default=3,
        help="the K epochs to update the policy")
    parser.add_argument("--norm-adv", type=lambda x: bool(strtobool(x)), default=True, nargs="?", const=True,
        help="Toggles advantages normalization")
    parser.add_argument("--clip-coef", type=float, default=0.2,
        help="the surrogate clipping coefficient")
    parser.add_argument("--clip-vloss", type=lambda x: bool(strtobool(x)), default=True, nargs="?", const=True,
        help="Toggles whether or not to use a clipped loss for the value function, as per the paper.")
    parser.add_argument("--ent-coef", type=float, default=0.01,
        help="coefficient of the entropy")
    parser.add_argument("--vf-coef", type=float, default=0.5,
        help="coefficient of the value function")
    parser.add_argument("--max-grad-norm", type=float, default=0.5,
        help="the maximum norm for the gradient clipping")
    parser.add_argument("--target-kl", type=float, default=None,
        help="the target KL divergence threshold")
    parser.add_argument("--repo-id", type=str, default="MRNH/ppo-CartPole-v1", help="id of the model repository from the Hugging Face Hub {username/repo_name}")

    args = parser.parse_args()
    args.batch_size = int(args.num_envs * args.num_steps)
    args.minibatch_size = int(args.batch_size // args.num_minibatches)
    gym.envs.register(id=args.env_id, entry_point=CustomEnv)
    return args



def make_env(env_id, seed, idx, capture_video, run_name):
    def thunk():
        env = gym.make(env_id)
        env = gym.wrappers.RecordEpisodeStatistics(env)
        if capture_video:
            if idx == 0:
                env = gym.wrappers.RecordVideo(env, f"videos/{run_name}")
        env.seed(seed)
        env.action_space.seed(seed)
        env.observation_space.seed(seed)
        return env

    return thunk


import asyncio
f=open("steps.txt","w")

async def socket_connection(ws, path):
    global observation_queue
    async for message in ws:
      custom_event = json.loads(message)
      if custom_event.get('type') == 'observationEvent':
          obs_app= [float(x) for x in custom_event.get('detail', {}).get('vector')[1:-1].split(",")]
          if observation_queue.qsize() == 0:
              await observation_queue.put(np.array(obs_app))
              print("RECEVING--->",observation_queue)
               
         
async def process_updates(ws, path, args):       
    global observation_queue
	# ALGO Logic: Storage setup
    obs = torch.zeros((args.num_steps, args.num_envs) + envs.single_observation_space.shape).to(device)
    actions = torch.zeros((args.num_steps, args.num_envs) + envs.single_action_space.shape).to(device)
    logprobs = torch.zeros((args.num_steps, args.num_envs)).to(device)
    rewards = torch.zeros((args.num_steps, args.num_envs)).to(device)
    dones = torch.zeros((args.num_steps, args.num_envs)).to(device)
    values = torch.zeros((args.num_steps, args.num_envs)).to(device)
    step_to_observ=[]
    # TRY NOT TO MODIFY: start the game
    global_step = 0
    start_time = time.time()
    
    try:
        observation = await observation_queue.get()  # Get observation from the queue
        print("SENDING--->",observation)
        #f.write("process_updates: "+str(observation))
        #f.write("\n")
        #f.write("\n")
        app_observation=observation[0]
        observation=observation[1:]
        step_to_observ.append(app_observation)
    except asyncio.CancelledError:
        print("err")
        
    envs.set_obs(observation)
    
    next_obs = torch.Tensor(envs.reset()).to(device)
    next_done = torch.zeros(args.num_envs).to(device)
    num_updates = args.total_timesteps // args.batch_size
    
    for update in range(1, num_updates + 1):
		##0. set up optimizer ##
        step_to_observ=[]

        if args.anneal_lr:
            frac = 1.0 - (update - 1.0) / num_updates
            lrnow = frac * args.learning_rate
            optimizer.param_groups[0]["lr"] = lrnow
        
        
        #############1. simply get the [obs,dones,actions,logprobs,rewards] for each steps ###############
        for step in range(0, args.num_steps):
            global_step += 1 * args.num_envs
            obs[step] = next_obs
            dones[step] = next_done
        
            # ALGO LOGIC: action logic
            with torch.no_grad():
                action, logprob, _, value = agent.get_action_and_value(next_obs)
                values[step] = value.flatten()
            actions[step] = action
            logprobs[step] = logprob
            
            #RECEIVE MESSAGE 
            """
            async for message in ws:
                custom_event = json.loads(message)
                if custom_event.get('type') == 'observationEvent':
                    obs_app= [float(x) for x in custom_event.get('detail', {}).get('vector')[1:-1].split(",")]
                    if observation_queue.qsize() == 0:
                        await observation_queue.put(np.array(obs_app)) 
            """
            # READ AND GET FROM QUEUE
            try:
                observation = await observation_queue.get()
                while observation[0]==app_observation:
                    continue
                app_observation=observation[0]
                observation=observation[1:]
                step_to_observ.append(app_observation)
            except asyncio.CancelledError:
                print("errore")
                
            envs.set_obs(observation)
            
            next_obs, reward, done, info = envs.step(action.cpu().numpy())        
            await ws.send(json.dumps(envs.get_response()[0]))
            
            rewards[step] = torch.tensor(reward).to(device).view(-1)
            next_obs, next_done = torch.Tensor(next_obs).to(device), torch.Tensor(done).to(device)
            
            for item in info:
                if "episode" in item.keys():
                    print(f"global_step={global_step}, episodic_return={item['episode']['r']}")
                    writer.add_scalar("charts/episodic_return", item["episode"]["r"], global_step)
                    writer.add_scalar("charts/episodic_length", item["episode"]["l"], global_step)
                    break
            
        print(rewards.shape)
        print("training...")

        print(

            actions[0],
                values[0],
                obs[0]
        )

        final_state=[]
        for e in range(args.num_steps):
            final_state.append([
            torch.tensor(step_to_observ[e]),
            torch.tensor(rewards[e]),
            torch.tensor(actions[e]),
            torch.tensor(values[e]),
            torch.tensor(obs[e]),
            torch.tensor(logprobs[e])])
        final_state = sorted(final_state, key=lambda x: x[0])


        for s in range(len(step_to_observ)):
            if final_state[s][4].squeeze()[0]==0 and final_state[s][4].squeeze()[1]==0:
                break
            else:
                final_state[s][1]=torch.tensor(0)
                final_state[s][2]=torch.tensor(0)
                final_state[s][3]=torch.tensor(3)
                final_state[s][4]=torch.zeros([2])
                final_state[s][5]=torch.tensor(0)

        step_to_observ=[]
        rewards=[]
        actions=[]
        values=[]
        obs=[]
        logprobs=[]
        prev_dist=0

        for s in final_state:
            if s[0] not in step_to_observ:
                if not obs:
                    dist_new=(0-35*35)+(0-35*35)
                else:
                    dist_new=(s[4].squeeze()[0]*s[4].squeeze()[0]-35*35)+(s[4].squeeze()[1]*s[4].squeeze()[1]-35*35)
                if prev_dist==0:
                    prev_dist=dist_new
                else:
                    dist_new=(obs[-1][0]*obs[-1][0]-35*35)+(obs[-1][1]*obs[-1][1]-35*35)

                if dist_new>-1000:
                    dist_new+=500
                if dist_new>2000:
                    dist_new+=800
                if dist_new>3000:
                    dist_new+=1200
                rewards.append(dist_new)
                
                step_to_observ.append(s[0].squeeze())
                actions.append(s[2].squeeze())
                values.append(s[3].squeeze())
                obs.append(s[4].squeeze())
                logprobs.append(s[5].squeeze())


        

        step_to_observ=torch.stack(step_to_observ)
        rewards=torch.tensor(rewards).type(torch.float).to(device)
        actions=torch.stack(actions).type(torch.float)
        values=torch.stack(values).type(torch.float)
        obs=torch.stack(obs)
        logprobs=torch.stack(logprobs)


        print(values.shape)

        for e in range(args.num_steps):
            #f.write("\n"+str(dones[e])+"\n")
            f.write("STEP: "+str(step_to_observ[e].item())+" "
                    "reward: "+str(rewards[e])+" "+
                    "actions: "+str(actions[e])+" "+
                    "values: "+str(values[e])+" "+
                    "observation: "+str(obs[e])+" "+
                    "logprobs: "+str(logprobs[e]))
            f.write("\n")
        f.write("*"*50)
        f.write("\n")
        f.write("\n")

        await ws.send(json.dumps({'message': "reset"}))

        #########2. training really starts here ###################

        with torch.no_grad():
            next_value = agent.get_value(next_obs).reshape(1, -1)
            if args.gae:
                advantages = torch.zeros_like(rewards).to(device)
                lastgaelam = 0
                for t in reversed(range(args.num_steps)):
                    if t == args.num_steps - 1:
                        nextnonterminal = 1.0 - next_done
                        nextvalues = next_value
                    else:
                        nextnonterminal = 1.0 - dones[t + 1]
                        nextvalues = values[t + 1]
                    delta = rewards[t] + args.gamma * nextvalues * nextnonterminal - values[t]
                    advantages[t] = lastgaelam = delta + args.gamma * args.gae_lambda * nextnonterminal * lastgaelam
                returns = advantages + values
            else:
                returns = torch.zeros_like(rewards).to(device)
                for t in reversed(range(args.num_steps)):
                    if t == args.num_steps - 1:
                        nextnonterminal = 1.0 - next_done
                        next_return = next_value
                    else:
                        nextnonterminal = 1.0 - dones[t + 1]
                        next_return = returns[t + 1]
                    returns[t] = rewards[t] + args.gamma * nextnonterminal * next_return
                advantages = returns - values
        
        # flatten the batch
        b_obs = obs.reshape((-1,) + envs.single_observation_space.shape)
        b_logprobs = logprobs.reshape(-1)
        b_actions = actions.reshape((-1,) + envs.single_action_space.shape)
        b_advantages = advantages.reshape(-1)
        b_returns = returns.reshape(-1)
        b_values = values.reshape(-1)
        
        # Optimizing the policy and value network
        b_inds = np.arange(args.batch_size)
        clipfracs = []
        for epoch in range(args.update_epochs):
            np.random.shuffle(b_inds)
            for start in range(0, args.batch_size, args.minibatch_size):
                end = start + args.minibatch_size
                mb_inds = b_inds[start:end]
        
                _, newlogprob, entropy, newvalue = agent.get_action_and_value(b_obs[mb_inds], b_actions.long()[mb_inds])
                logratio = newlogprob - b_logprobs[mb_inds]
                ratio = logratio.exp()
        
                with torch.no_grad():
                    # calculate approx_kl http://joschu.net/blog/kl-approx.html
                    old_approx_kl = (-logratio).mean()
                    approx_kl = ((ratio - 1) - logratio).mean()
                    clipfracs += [((ratio - 1.0).abs() > args.clip_coef).float().mean().item()]
        
                mb_advantages = b_advantages[mb_inds]
                if args.norm_adv:
                    mb_advantages = (mb_advantages - mb_advantages.mean()) / (mb_advantages.std() + 1e-8)
        
                # Policy loss
                pg_loss1 = -mb_advantages * ratio
                pg_loss2 = -mb_advantages * torch.clamp(ratio, 1 - args.clip_coef, 1 + args.clip_coef)
                pg_loss = torch.max(pg_loss1, pg_loss2).mean()
        
                # Value loss
                newvalue = newvalue.view(-1)
                if args.clip_vloss:
                    v_loss_unclipped = (newvalue - b_returns[mb_inds]) ** 2
                    v_clipped = b_values[mb_inds] + torch.clamp(
                        newvalue - b_values[mb_inds],
                        -args.clip_coef,
                        args.clip_coef,
                    )
                    v_loss_clipped = (v_clipped - b_returns[mb_inds]) ** 2
                    v_loss_max = torch.max(v_loss_unclipped, v_loss_clipped)
                    v_loss = 0.5 * v_loss_max.mean()
                else:
                    v_loss = 0.5 * ((newvalue - b_returns[mb_inds]) ** 2).mean()
        
                entropy_loss = entropy.mean()
                loss = pg_loss - args.ent_coef * entropy_loss + v_loss * args.vf_coef
        
                optimizer.zero_grad()
                loss.backward()
                nn.utils.clip_grad_norm_(agent.parameters(), args.max_grad_norm)
                optimizer.step()

            if args.target_kl is not None:
                if approx_kl > args.target_kl:
                    break
                    
        y_pred, y_true = b_values.cpu().numpy(), b_returns.cpu().numpy()
        var_y = np.var(y_true)
        explained_var = np.nan if var_y == 0 else 1 - np.var(y_true - y_pred) / var_y
        
        # TRY NOT TO MODIFY: record rewards for plotting purposes
        writer.add_scalar("charts/learning_rate", optimizer.param_groups[0]["lr"], global_step)
        writer.add_scalar("losses/value_loss", v_loss.item(), global_step)
        writer.add_scalar("losses/policy_loss", pg_loss.item(), global_step)
        writer.add_scalar("losses/entropy", entropy_loss.item(), global_step)
        writer.add_scalar("losses/old_approx_kl", old_approx_kl.item(), global_step)
        writer.add_scalar("losses/approx_kl", approx_kl.item(), global_step)
        writer.add_scalar("losses/clipfrac", np.mean(clipfracs), global_step)
        writer.add_scalar("losses/explained_variance", explained_var, global_step)
        print("SPS:", int(global_step / (time.time() - start_time)))
        writer.add_scalar("charts/SPS", int(global_step / (time.time() - start_time)), global_step)
        
        
        #_, _, _, _ = envs.step(torch.tensor([7]).cpu().numpy())
        """
        try:
            observation = await observation_queue.get()  
            print(observation)
        except asyncio.CancelledError:
            print("errore")
        """
        #await ws.send(json.dumps(envs.get_response()[0]))
        #print("--------------------------------------------",envs.get_response()[0])
        
    
    print("END TRAINING")
    envs.close()
    writer.close()

    # Create the evaluation environment
    eval_env = gym.make(args.env_id)
    
    hub.package_to_hub(repo_id = args.repo_id,
                model = agent, # The model we want to save
                hyperparameters = args,
                eval_env = gym.make(args.env_id),
                logs= f"runs/{run_name}",
                )


async def main(ws, path, args):
    message_task = asyncio.create_task(socket_connection(ws, path))
    updates_task = asyncio.create_task(process_updates(ws,path,args))
    await asyncio.gather(message_task, updates_task)
    #await asyncio.gather(socket_connection(ws, path), process_updates(ws, path, args))
    

    
if __name__ == "__main__":
    args = parse_args()
    run_name = f"{args.env_id}__{args.exp_name}__{args.seed}__{int(time.time())}"
    if args.track:
        wandb.init(project=args.wandb_project_name,
        entity=args.wandb_entity,
        sync_tensorboard=True,
        config=vars(args),
        name=run_name,
        monitor_gym=True,
        save_code=True,)
        
    writer = SummaryWriter(f"runs/{run_name}")
    writer.add_text(
    	"hyperparameters",
    	"|param|value|\n|-|-|\n%s" % ("\n".join([f"|{key}|{value}|" for key, value in vars(args).items()])),
    )
    
    # TRY NOT TO MODIFY: seeding
    random.seed(args.seed)
    np.random.seed(args.seed)
    torch.manual_seed(args.seed)
    torch.backends.cudnn.deterministic = args.torch_deterministic
    
    device = torch.device("cuda" if torch.cuda.is_available() and args.cuda else "cpu")
    # env setup
    envs = CustomSyncVectorEnv(
    	[make_env(args.env_id, args.seed + i, i, args.capture_video, run_name) for i in range(args.num_envs)]
    )
    assert isinstance(envs.single_action_space, gym.spaces.Discrete), "only discrete action space is supported"
    
    agent = Agent(envs).to(device)
    optimizer = optim.Adam(agent.parameters(), lr=args.learning_rate, eps=1e-5)
	
    """SOCKET"""
    start_server = websockets.serve(lambda ws, path: main(ws, path, args), "127.0.0.1", 15002)
    asyncio.get_event_loop().run_until_complete(start_server)
    asyncio.get_event_loop().run_forever()
    """"""
    

# TORUN!!!!
"""
!huggingface-cli login
%%capture
!apt install python-opengl
!apt install ffmpeg
!apt install xvfb
!apt install swig cmake
!pip install pyglet==1.5
!pip3 install pyvirtualdisplay
!pip install gym==0.22
!pip install imageio-ffmpeg
!pip install huggingface_hub
!pip install gym[box2d]==0.22


# Virtual display
from pyvirtualdisplay import Display

virtual_display = Display(visible=0, size=(1400, 900))
virtual_display.start()

!python ppo.py --env-id="LunarLander-v2" --repo-id="MRNH/LunarLander-v2" --total-timesteps=100000
python3 ppo.py --env-id="myenv-v1" --repo-id="MRNH/myenv-v1" --total-timesteps=100000

"""
