"""
from flask import Flask, request, jsonify
from flask_cors import CORS
import torch
from transformers import MBartForConditionalGeneration, MBart50TokenizerFast
import transformers
from torch import nn
import numpy as np
from torch import nn

app = Flask(__name__)
CORS(app, resources={r"/*": {"origins": "*"}})

@app.route('/file_write.py', methods=['POST'])
def error():
    query = request.get_json().get('query')
    response = {'message': "left"}
    resp = jsonify(response)
    resp.headers['Access-Control-Allow-Origin'] = '*'
    print(resp)
    return resp

if __name__ == '__main__':
    app.run(host='127.0.0.1', port=15000, debug=True)


from flask import Flask
from flask_sockets import Sockets
import json

app = Flask(__name__)
sockets = Sockets(app)

@sockets.route('/')
def socket_connection(ws):
    while not ws.closed:
        message = ws.receive()
        if message:
            custom_event = json.loads(message)
            if custom_event.get('type') == 'observationEvent':
                observation = custom_event.get('detail', {}).get('vector')
                if observation:
                    print("Received observation:", observation)

if __name__ == '__main__':
    from gevent import pywsgi
    from geventwebsocket.handler import WebSocketHandler
    server = pywsgi.WSGIServer(('localhost', 15000), app, handler_class=WebSocketHandler)
    server.serve_forever()


import socket

def start_socket_server():
    host = '127.0.0.1'  
    port = 15002 

    # Create a socket object
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    # Bind the socket to the host and port
    server_socket.bind((host, port))

    # Listen for incoming connections
    server_socket.listen(1)
    print(f"Listening on {host}:{port}")

    # Accept a connection from a client
    client_socket, client_address = server_socket.accept()
    print(f"Connection established with {client_address}")

    try:
        while True:
            # Receive data from the client
            data = client_socket.recv(1024)
            if not data:
                break

            received_data = data.decode('utf-8')
            print(f"Received data ---------------->: {received_data}")

    except KeyboardInterrupt:
        pass

    finally:
        # Close the client socket
        client_socket.close()
        # Close the server socket
        server_socket.close()

if __name__ == '__main__':
    start_socket_server()
"""

import asyncio
import websockets
import json
async def socket_connection(ws, path):
    async for message in ws:
        custom_event = json.loads(message)
        if custom_event.get('type') == 'observationEvent':
            observation = custom_event.get('detail', {}).get('vector')
            if observation:
                print("Received observation:", observation)

if __name__ == '__main__':
    start_server = websockets.serve(socket_connection, "127.0.0.1", 15002)
    asyncio.get_event_loop().run_until_complete(start_server)
    asyncio.get_event_loop().run_forever()
    
    
    

   
